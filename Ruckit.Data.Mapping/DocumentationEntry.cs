﻿using System;
using System.Xml.Serialization;

namespace Ruckit.Data.Mapping {
    [XmlRoot(ElementName = "description", Namespace = MappingModel.DocumentationNamespace)]
    public sealed class DocumentationEntry {
        [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string Language { get; set;  }
        [XmlText]
        public string Content { get; set;  }
    }
}
