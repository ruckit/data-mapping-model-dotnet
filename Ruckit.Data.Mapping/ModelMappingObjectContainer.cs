﻿using System;
using System.Collections.ObjectModel;

namespace Ruckit.Data.Mapping {
    internal sealed class ModelMappingObjectContainer<T> : Collection<T> where T: MappingModelDescriptionObject.IMappingObject {
        internal ModelMappingObjectContainer(MappingModelDescriptionObject.IMappingObject owner) {
            this.Owner = owner;
        }

        public MappingModelDescriptionObject.IMappingObject Owner { get; }

        protected override void SetItem(int index, T item) {
            if (item.Parent != null && item.Parent != this.Owner) {
                throw new NotSupportedException($"The attempt to add object failed because the object is part of another model.");
            }
            this[index].Parent = null;
            base.SetItem(index, item);
            item.Parent = this.Owner;
        }
        protected override void InsertItem(int index, T item) {
            if (item.Parent != null && item.Parent != this.Owner) {
                throw new NotSupportedException($"The attempt to add object failed because the object is part of another model.");
            }
            base.InsertItem(index, item);
            item.Parent = this.Owner;
        }
        protected override void RemoveItem(int index) {
            this[index].Parent = null;
            base.RemoveItem(index);
        }
        protected override void ClearItems() {
            foreach (var i in this) i.Parent = null;
            base.ClearItems();
        }
    }
}
