﻿using System;
using System.Collections.Generic;

namespace Ruckit.Data.Mapping {
    partial class MappingModelDescriptionObject {
        internal interface IMappingTransformer {
            void Transform(Binding.IMapBinding source, Binding.IMapBinding target);
        }

        internal interface IMappingObject {
            IMappingObject Parent { get; set; }
        }
    }
}
