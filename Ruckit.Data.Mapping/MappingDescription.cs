﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Serialization;
using Ruckit.Data.Mapping.Binding;
using System.Linq;

namespace Ruckit.Data.Mapping {
    public class MappingDescription : MappingModelDescriptionObject, MappingModelDescriptionObject.IMappingTransformer {
        public MappingDescription() {
            this.ValueMappings = this.CreateChildCollection<ValueMappingDescription>();
            this.MappingReferences = this.CreateChildCollection<MappingReferenceTransformDescription>();
        }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "value", Type = typeof(ValueMappingDescription))]
        public Collection<ValueMappingDescription> ValueMappings { get; }
        [XmlElement(ElementName = "mapping", Type = typeof(MappingReferenceTransformDescription))]
        public Collection<MappingReferenceTransformDescription> MappingReferences { get; }

        public void Map(Binding.IMapBinding source, Binding.IMapBinding target) {
            ((IMappingTransformer)this).Transform(source, target);
        }
        public object Map(object source) {
            var s = Binding.Objects.ObjectMapBinding.FromObject(source);
            var t = new Binding.Objects.ObjectMapDictionaryBinding();
            ((IMappingTransformer)this).Transform(s, t);
            return t;
        }
        void IMappingTransformer.Transform(IMapBinding source, IMapBinding target) {

            if (!(target is IMapDictionaryBinding dictTarget)) {
                throw new MappingException($"Binding target '{target}' could not be mapped to because it is not a dictionary.");
            }

            foreach (var mapping in this.ValueMappings) {
                var sourceDict = source as IMapDictionaryBinding;

                if (!string.IsNullOrEmpty(mapping.SourceKey) && sourceDict == null) {
                    throw new MappingException($"A source key '{mapping.SourceKey}' is specified, but the source value is not a dictionary.");

                }

                var mapSourceValue = string.IsNullOrEmpty(mapping.SourceKey) ? sourceDict : sourceDict[mapping.SourceKey];
                var mapTargetValue = dictTarget.BindDictionary(mapping.TargetKey);

                ((IMappingTransformer)mapping).Transform(mapSourceValue, mapTargetValue);


            }

        }



        //        //public object Map(object sourceObject) {
        //        //    var context = new MappingContext();
        //        //    context.EnterScope(sourceObject);
        //        //   return  this.Transform(context);
        //        //}
        //        //protected override object Transform(MappingContext context) {
        //        //    var dict = new Dictionary<string, object>();

        //        //    foreach (var mapping in this.ValueMappings) {
        //        //        var value = ((IMappingTransformer)mapping).Transform(context);
        //        //        dict[mapping.TargetKey] = value;
        //        //    }

        //        //    foreach (var mappingRef in this.MappingReferences) {
        //        //        var value = ((IMappingTransformer)mappingRef).Transform(context);
        //        //        if (value is IDictionary innerDict) {
        //        //            foreach (var key in innerDict.Keys) {
        //        //                dict[key.ToString()] = innerDict[key];
        //        //            }
        //        //        }
        //        //    }

        //        //    return dict;
        //        //} 
        //    }
        //}
        //}
        //}

        //return dict;
    }
}
