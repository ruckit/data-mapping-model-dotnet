﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruckit.Data.Mapping.Binding {
    public enum MapBindingKind {
        None,
        Value,
        Dictionary,
        Array
    }

    public interface IMapBinding {
        
    }
    public interface IMapBindingContainer : IMapBinding {
        int Count { get; }
    }
    public interface IMapArrayBinding : IMapBindingContainer {
        IMapBinding this[int index] { get; }

        void Unbind(int index);

        IMapValueBinding BindValue();
        IMapDictionaryBinding BindDictionary();
        IMapArrayBinding BindArray();
    }
    public interface IMapDictionaryBinding  : IMapBindingContainer {
        IMapBinding this[string key] { get; }

        void Unbind(string key);
        IMapValueBinding BindValue(string key);
        IMapDictionaryBinding BindDictionary(string key);
        IMapArrayBinding BindArray(string key);
    }

    public interface IMapValueBinding : IMapBinding {
        object Value { get; set; }
        Type PreferredType { get; }
   }

}
