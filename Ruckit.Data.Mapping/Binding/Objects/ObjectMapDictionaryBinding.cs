﻿using System;
using Ruckit.Data.Mapping.Binding;
using System.Collections.Generic;


namespace Ruckit.Data.Mapping.Binding.Objects {

    public sealed class ObjectMapDictionaryBinding : ObjectMapBinding, IMapDictionaryBinding {
        private Dictionary<string, object> _values = new Dictionary<string, object>();

        public IMapBinding this[string key] {
            get {
                object value;
                if (!_values.TryGetValue(key, out value)) {
                    return null;
                }
                return (IMapBinding)value;
            

            }
        }

        public int Count => _values.Count;

        public IMapArrayBinding BindArray(string key) {
            var binding = new ObjectMapArrayBinding();
            _values[key] = binding;
            return binding;
        }

        public IMapDictionaryBinding BindDictionary(string key) {
            var binding = new ObjectMapDictionaryBinding();
            _values[key] = binding;
            return binding;
        }

        public IMapValueBinding BindValue(string key) {
            var binding = new ObjectMapValueBinding();
            _values[key] = binding;
            return binding;
        }

        public void Unbind(string key) {
            _values.Remove(key);
        }
    }

}