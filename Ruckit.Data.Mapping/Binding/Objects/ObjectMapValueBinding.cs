﻿using System;
using Ruckit.Data.Mapping.Binding;
using System.Collections;

namespace Ruckit.Data.Mapping.Binding.Objects {

    public sealed class ObjectMapValueBinding : ObjectMapBinding, IMapValueBinding {
        public object Value { get; set; }
        public Type PreferredType { get { return null; } }
    }
}