﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruckit.Data.Mapping.Binding.Objects {
    public abstract class ObjectMapBinding {
        public static IMapBinding FromObject(object obj, IMapBinding parent = null) {

            switch (obj) {
                case IDictionary dict:
                    if (parent == null) {
                        parent = new ObjectMapDictionaryBinding();
                    }

                    foreach (var key in dict.Keys) {
                        var value = dict[key];
                        var innerValue = FromObject(value, parent);
                        ((IMapDictionaryBinding)parent).BindValue(key.ToString()).Value = innerValue ?? value;
                    }
                    return parent;
                case IList array:
                    if (parent == null) {
                        parent = new ObjectMapArrayBinding();
                    }
                    foreach (var item in array) {
                        var value = FromObject(item, parent);
                        ((IMapArrayBinding)parent).BindValue().Value = value ?? item;
                    }
                    return parent;
                default:
                    return null;

            }
        }
    }
}
