﻿using System;
using Ruckit.Data.Mapping.Binding;
using System.Collections.Generic;

namespace Ruckit.Data.Mapping.Binding.Objects {

    public sealed class ObjectMapArrayBinding : ObjectMapBinding, IMapArrayBinding {
        private List<object> _items = new List<object>();

        public IMapBinding this[int key] {
            get {
                object value = _items[key];
                return (IMapBinding)value;
            }
        }

        public int Count => _items.Count;

        public IMapArrayBinding BindArray() {
            var binding = new ObjectMapArrayBinding();
            _items.Add(binding);
            return binding;
        }

        public IMapDictionaryBinding BindDictionary() {
            var binding = new ObjectMapDictionaryBinding();
            _items.Add(binding);
            return binding;
        }

        public IMapValueBinding BindValue() {
            var binding = new ObjectMapValueBinding();
            _items.Add(binding);
            return binding;
        }

        public void Unbind(int index) {
            _items.RemoveAt(index);
        }
    }
}