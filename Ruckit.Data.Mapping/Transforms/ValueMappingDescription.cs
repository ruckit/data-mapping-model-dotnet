﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Serialization;
using System.Linq;
using System.Collections.ObjectModel;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    [XmlRoot(ElementName = "value", Namespace = MappingModel.MappingModelNamespace)]
    public class ValueMappingDescription : ValueTransformDescription {
        public ValueMappingDescription() {
            this.Transformations = this.CreateChildCollection<ValueTransformDescription>();
        }

        [XmlAttribute(AttributeName = "from")]
        public string SourceKey { get; set; }
        [XmlAttribute(AttributeName = "to")]
        public string TargetKey { get; set; }
        [XmlAttribute(AttributeName = "required")]
        public bool IsRequired { get; set; }

        [XmlElement(ElementName = "value", Type = typeof(ValueMappingDescription))]
        [XmlElement(ElementName = "number", Type = typeof(NumberTransformDescription))]
        [XmlElement(ElementName = "date", Type = typeof(DateTransformDescription))]
        [XmlElement(ElementName = "timestamp", Type = typeof(TimestampTransformDescription))]
        [XmlElement(ElementName = "boolean", Type = typeof(BooleanTransformDescription))]
        [XmlElement(ElementName = "mapping", Type = typeof(MappingReferenceTransformDescription))]
        public Collection<ValueTransformDescription> Transformations { get; }
        public IEnumerable<ValueMappingDescription> ValueMappings {
            get {
                if (this.Transformations.Count > 0 && this.Transformations.All(t => t is ValueMappingDescription)) {
                    return this.Transformations.Cast<ValueMappingDescription>();
                }

                return Enumerable.Empty<ValueMappingDescription>();
            }
        }

        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            throw new NotImplementedException();
        }
        protected override void Transform(IMapBinding source, IMapBinding target) {

        }

       // protected override object Transform(MappingContext context) {
            //if (this.ValueMappings.Any()) {
            //    var tdict = new Dictionary<string, object>();

            //    foreach (var mapping in this.ValueMappings) {
            //        var value = ((IMappingTransformer)mapping).Transform(context);
            //        tdict[mapping.TargetKey] = value;
            //    }

            //    return tdict;
            //}

            //if (this.SourceKey == null && this.TargetKey == null) {
            //    return context.Value;
            //}

            //if ((context.Value is IDictionary dict)) {
            //    if (this.SourceKey != null && (!dict.Contains(this.SourceKey) && this.IsRequired)) {
            //        throw new InvalidOperationException($"The source key '{this.SourceKey}' was not found in the dictionary and this is a required value.");
            //    }

            //    var value = this.SourceKey == null ? dict : dict[this.SourceKey];
            //    try {
            //        context.EnterScope(value);
            //        foreach (var transformer in this.Transformations.Cast<MappingModelDescriptionObject.IMappingTransformer>()) {
            //            try {
            //                context.EnterScope(value);
            //                value = transformer.Transform(context);
            //            } finally {
            //                context.ExitScope();
            //            }
            //        }
            //        return value;
            //    } finally {
            //        context.ExitScope();
            //    }
            //}

            //throw new InvalidOperationException($"The source key '{this.SourceKey}' could not be retrieved because the context value is not a dictionary: {context.Value}");

       // }
    }
}
