﻿using System;
using System.Xml.Serialization;
using System.Linq;
using System.Collections;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    public class MappingReferenceTransformDescription : ValueTransformDescription {
        public MappingReferenceTransformDescription() {
        }

        [XmlAttribute(AttributeName = "ref")]
        public string MappingName { get; set; }

        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            throw new NotImplementedException();
        }
        protected override void Transform(IMapBinding source, IMapBinding target) {
            var model = this.Model;
            if (model == null) {
                throw new InvalidOperationException($"Unable to get the referenced mapping '{this.MappingName}' because this object has not been added to any mapping model.");
            }

            var mapping = model[this.MappingName];
            if (mapping == null) {
                throw new InvalidOperationException($"Unable to find the referenced mapping '{this.MappingName}' in the model.");
            }

           ((IMappingTransformer)mapping).Transform(source,target);

        }
    }
}
