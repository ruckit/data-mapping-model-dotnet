﻿using System;
using System.Xml.Serialization;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    public enum NumberTransformKind {
        [XmlEnum("auto")]
        Automatic,
        [XmlEnum("integer")]
        Integer,
        [XmlEnum("float")]
        Float,
        [XmlEnum("decimal")]
        Decimal
    }
    [XmlRoot(ElementName = "number", Namespace = MappingModel.MappingModelNamespace)]
    public sealed class NumberTransformDescription : ValueTransformDescription {
        public NumberTransformDescription() {
        }

        [XmlAttribute(AttributeName = "type")]
        public NumberTransformKind Kind { get; set; }

        [XmlAttribute(AttributeName = "strict")]
        public bool IsStrict { get; set; }

        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            // TODO: Check the IsStrict property.

            string strNumber = source.Value.ToString().Trim();

            var kind = this.Kind;
            if (kind == NumberTransformKind.Automatic) {
                if (strNumber.StartsWith("$", StringComparison.OrdinalIgnoreCase)) {
                    kind = NumberTransformKind.Decimal;
                    strNumber.Remove(0, 1);
                } else if (strNumber.Contains(".")) {
                    kind = NumberTransformKind.Float;
                } else {
                    kind = NumberTransformKind.Integer;
                }
            }

            switch (kind) {
                case NumberTransformKind.Integer:
                    target.Value = int.Parse(strNumber);
                    break;
                case NumberTransformKind.Float:
                    target.Value = double.Parse(strNumber);
                    break;
                case NumberTransformKind.Decimal:
                    target.Value = decimal.Parse(strNumber);
                    break;
                default:
                    throw new Exception();
            }
        }
    }
}
