﻿using System;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    public abstract class  ValueTransformDescription : MappingModelDescriptionObject, MappingModelDescriptionObject.IMappingTransformer
    {
        protected ValueTransformDescription() { }

        protected abstract void Transform(IMapValueBinding source, IMapValueBinding target);
        
        protected virtual void Transform(IMapBinding source, IMapBinding target) {
            if (source is IMapValueBinding smv && target is IMapValueBinding tmv) {
                this.Transform(smv, tmv);
                return;
            }

            throw new MappingException($"Unable to transform because either the source or destination are not value bindings.");
        }
        void IMappingTransformer.Transform(IMapBinding source, IMapBinding target)
        {
            this.Transform(source, target);
      
        }
    }
}
