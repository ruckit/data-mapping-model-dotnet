﻿using System;
using System.Xml;
using System.Xml.Serialization;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    [XmlRoot(ElementName = "date", Namespace = MappingModel.MappingModelNamespace)]
    public class DateTransformDescription : ValueTransformDescription {
        public DateTransformDescription() {
            
        }

        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set;  }
       
        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            if (source.Value is DateTime dateTime) {
                target.Value = dateTime;
                return;
            }

            var str = source.Value.ToString();
            if (this.Format == "xml") {
                target.Value =  XmlConvert.ToDateTime(str, XmlDateTimeSerializationMode.Utc);
                return;
            }

            DateTime parsed;
            if ((this.Format == "" || this.Format == "auto") && DateTime.TryParse(str, out parsed)) {
                target.Value = parsed;
            }

            if (DateTime.TryParseExact(str, this.Format, null, System.Globalization.DateTimeStyles.None, out parsed)) {
                target.Value = parsed;
            }

            throw new MappingException($"The date/time '{str}' could not be parsed using format '{this.Format}'.");
        }

    }
}
