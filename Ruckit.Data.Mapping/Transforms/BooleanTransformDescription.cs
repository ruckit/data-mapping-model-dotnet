﻿using System;
using System.Xml.Serialization;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {
    [XmlRoot(ElementName = "boolean", Namespace = MappingModel.MappingModelNamespace)]
    public sealed class BooleanTransformDescription : ValueTransformDescription {


        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            var text = (source.Value ?? "f").ToString().Trim().ToLower();
            target.Value = (text.StartsWith("t") || text.StartsWith("1") || text.StartsWith("y"));
        }

    }
}
