﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Ruckit.Data.Mapping.Binding;

namespace Ruckit.Data.Mapping {

    [XmlRoot(ElementName = "timestamp", Namespace = MappingModel.MappingModelNamespace)]
    public class TimestampTransformDescription : ValueTransformDescription {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public TimestampTransformDescription() {
        }

        [XmlIgnore]
        public DateTime Epoch {
            get {
                switch (this.RawEpoch) {
                    case "unix":
                        return UnixEpoch;
                    default:
                        return XmlConvert.ToDateTime(this.RawEpoch, XmlDateTimeSerializationMode.Utc);
                }
            }
            set {
                if (value == UnixEpoch) { 
                    this.RawEpoch = "unix";
                } else {
                    this.RawEpoch = XmlConvert.ToString(value, XmlDateTimeSerializationMode.Utc);
                }
            }
        }

        [XmlAttribute("epoch")]
        public string RawEpoch { get; set; }

        protected override void Transform(IMapValueBinding source, IMapValueBinding target) {
            var seconds = Convert.ToDouble(source.Value);
            target.Value = this.Epoch + TimeSpan.FromSeconds(seconds);
        }
    }
}
