﻿using System;
namespace Ruckit.Data.Mapping {
    public sealed class MapSettings {
        public MapSettings(Binding.IMapBinding source, Binding.IMapBinding target) {
            (this.SourceBinding, this.TargetBinding) = (source, target);
        }

        public Binding.IMapBinding SourceBinding { get; }
        public Binding.IMapBinding TargetBinding { get;  }
    }
}
