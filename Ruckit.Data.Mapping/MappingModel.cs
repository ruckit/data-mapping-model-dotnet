﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;
using System.Collections.ObjectModel;

namespace Ruckit.Data.Mapping {
    [XmlRoot(ElementName = "definitions", Namespace = MappingModel.MappingModelNamespace)]
    public sealed class MappingModel : MappingModelDescriptionObject {
        public const string MappingModelNamespace = "http://www.goruckit.com/schemas/data-mapping-model.xsd";
        public const string DocumentationNamespace = "http://www.goruckit.com/schemas/simple-documentation.xsd";

        public MappingModel() {
            this.Mappings = CreateChildCollection<MappingDescription>();
        }

        [XmlElement(ElementName = "mapping")]
        public Collection<MappingDescription> Mappings { get; }

        public MappingDescription this[string name] {
            get {
                return this.Mappings.FirstOrDefault(m => m.Name == name);
            }
        }

        public static MappingModel Read(string filename) {
            using (var stream = File.OpenRead(filename)) {
                return Read(stream);
            }
        }

        public static MappingModel Read(Stream stream) {
            var validaingReader = XmlReader.Create(stream, new XmlReaderSettings() {
                Schemas = Schemas.Value,
                ValidationType = ValidationType.Schema,
                ValidationFlags = XmlSchemaValidationFlags.ProcessInlineSchema | XmlSchemaValidationFlags.ProcessIdentityConstraints | XmlSchemaValidationFlags.ReportValidationWarnings
            });


            var serializer = new XmlSerializer(typeof(MappingModel), MappingModel.MappingModelNamespace);
            var obj = serializer.Deserialize(validaingReader);
            return (MappingModel)obj;


        }

       public static readonly Lazy<XmlSchemaSet> Schemas = new Lazy<XmlSchemaSet>(() => {
            XmlSchema LoadSchema(string resourceName) {
                var assm = System.Reflection.Assembly.GetExecutingAssembly();
                using (var stream = assm.GetManifestResourceStream(resourceName)) {
                    XmlSchema schema = XmlSchema.Read(stream, HandleValidationEventHandler);
                   return schema;
                }
            }

            var s1 = LoadSchema("data-mapping-model-schema");
            var s2 = LoadSchema("simple-documentation-schema");

            var schemas = new XmlSchemaSet();
            schemas.Add(s1);
            schemas.Add(s2);

            return schemas;

        });

        static void HandleValidationEventHandler(object sender, ValidationEventArgs e) {
        }


    }
}
