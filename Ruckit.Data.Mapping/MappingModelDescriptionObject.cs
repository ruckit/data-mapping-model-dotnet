﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Serialization;

namespace Ruckit.Data.Mapping {
    public  abstract partial class MappingModelDescriptionObject : MappingModelDescriptionObject.IMappingObject {
        protected MappingModelDescriptionObject() { }

        [XmlArray(ElementName = "content", Namespace = MappingModel.DocumentationNamespace)]
        [XmlArrayItem(ElementName = "description",  Type = typeof(DocumentationEntry))]
        public List<DocumentationEntry> Documentation { get; } = new List<DocumentationEntry>();

        [XmlAnyAttribute]
        public XmlAttribute[] OtherAttributes { get; set;  }
        [XmlAnyElement]
        public XmlElement[] OtherElements { get; set; }

        protected Collection<T> CreateChildCollection<T>() where T: MappingModelDescriptionObject {
            return new ModelMappingObjectContainer<T>(this);
        }

        IMappingObject IMappingObject.Parent { get; set;  }

        public MappingModel Model {
            get {
                IMappingObject current = this;
                while (current != null) {
                    if (current is MappingModel m) {
                        return m;
                    }

                    current = ((IMappingObject)current).Parent;
                }
                return null;
            }
        }
    }
}