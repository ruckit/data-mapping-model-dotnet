﻿using System;
namespace Ruckit.Data.Mapping {
    public class MappingException : Exception {
        public MappingException(string message, Exception innerException = null) : base(message, innerException) {
        }
    }
}
