# Ruckit Data Mapping Framework

## What the hell is this, I hate XML!
This library makes it simple to remap tabular data (aka key/value pairs) from a 3rd party source
to the format /you/ need it in.  Driven by XML, and with its accopinaing XML schema, you'll find it
very easy to edit the mapping file. In suppported IDE's, intellisense, documentation popups, and errors will 
appear as you write..

## Why don't I just write this mapping in code?
Writing native code to map values is boring, tedious, and very prone to error. Furthermore, a complete redeploy is required if the 
incoming data changes.  A new mapping model can be loaded while the application is running and be used immediately.

## How can this framework possibly handle a case where the source data is a unix timestamp and I need an actual date/time?
Here's how that's done:

`````
<?xml version="1.0" encoding="utf-8"?>
<definitions name="test" xmlns="http://schemas.goruckit.com/data-adaptation">
  <map from="dob" to="Birthdate">
    <timestamp epoch="unix" />
  </map>
</definitions>
`````